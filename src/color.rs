use crate::sdl2;
use sdl2::pixels::Color;

// todo: make None a color, by providing an enum with only two values, namely
// None and a Color::RGB
pub const TRANSPARENCY: Color = Color::RGBA(0, 0, 0, 0);
pub const PALETTE: [Color; 8] = [
    Color::RGB(0, 0, 0),
    Color::RGB(255, 0, 0),
    Color::RGB(0, 255, 0),
    Color::RGB(0, 0, 255),
    Color::RGB(0, 255, 255),
    Color::RGB(255, 0, 255),
    Color::RGB(255, 255, 0),
    Color::RGB(255, 255, 255),
];
