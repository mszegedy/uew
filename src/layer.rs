use std::collections::HashMap;

use crate::na;
use na::{
    Complex,
    UnitComplex,
    Point2,
    Vector2,
    Matrix3,
    Translation,
    Similarity2,
    Affine2,
};

use crate::sdl2;
use sdl2::render::{
    Canvas,
    Texture,
    RenderTarget,
    BlendMode,
};
use sdl2::gfx::primitives::DrawRenderer;
use sdl2::pixels::Color;
use sdl2::mouse::MouseState;

use crate::color;
use crate::path;
use crate::View;

const SQRT_2: f32 = 1.4142135623730951f32;
pub const LAYER_PIXEL_FORMAT: sdl2::pixels::PixelFormatEnum =
    sdl2::pixels::PixelFormatEnum::RGBA8888;

fn clear_texture<T: RenderTarget>(
    texture: &mut Texture, canvas: &mut Canvas<T>
) {
    canvas.set_draw_color(color::TRANSPARENCY);
    canvas.with_texture_canvas(texture, |c| { c.clear(); }).unwrap();
}

/// uses `scale` to create a set of four coordinates delimiting the diamond
/// associated with a particular xy coordinate. the weird format where you put
/// the x values in the first array and the y values in the second array is
/// because that's how the `filled_polygon` drawing function takes it.
fn xy_to_diamond(v: Point2<f32>, scale: f32) -> ([i16; 4], [i16; 4]) {
    let total_scale = scale / SQRT_2;
    ([v.x as i16,
      (v.x - total_scale).round() as i16,
      v.x as i16,
      (v.x + total_scale).round() as i16],
     [v.y as i16,
      (v.y - total_scale).round() as i16,
      (v.y - SQRT_2 * scale).round() as i16,
      (v.y - total_scale).round() as i16])
}

pub struct SDLGeometry {
    origin: Vector2<f32>,
    window_size: Vector2<f32>,
    ij_to_sdl: Affine2<f32>,
    total_scale: f32,
}

pub struct Layer {
    ij_to_xy: Similarity2<f32>,
    points: HashMap<[i16; 2], usize>,
}

impl Layer {

    pub fn new(origin: Point2<f32>, scale: f32) -> Layer {
        Layer {
            ij_to_xy: Similarity2::from_parts(
                Translation::from(origin.coords),
                UnitComplex::from_complex(Complex { re: 1f32, im: 1f32}),
                scale,
            ),
            points: HashMap::new(),
        }
    }

    pub fn origin(&self) -> Vector2<f32> {
        self.ij_to_xy.isometry.translation.vector
    }

    pub fn sdl_geometry(&self, view: &View) -> SDLGeometry {
        let (width, height) = view.canvas.output_size().unwrap();
        let window_size = Vector2::new(width as f32, height as f32);

        SDLGeometry {
            origin: self.origin() + view.origin + window_size/2f32,
            window_size: window_size.clone(),
            ij_to_sdl: Affine2::from_matrix_unchecked(
                Matrix3::new(
                    view.scale, 0f32, view.origin.x + window_size.x/2f32,
                    0f32, -view.scale, view.origin.y + window_size.y/2f32,
                    0f32, 0f32, 1f32,
                )
            ) * self.ij_to_xy,
            total_scale: self.ij_to_xy.scaling() * view.scale,
        }
    }

    pub fn add_point(&mut self, i: i16, j: i16, c: usize) {
        self.points.insert([i, j], c);
    }

    pub fn click(
        &mut self,
        view: &View,
        mouse_state: &MouseState,
    ) {
        let sdl_geometry = self.sdl_geometry(&view);
        let mouse_pos = Point2::new(mouse_state.x() as f32,
                                    mouse_state.y() as f32);
        let mouse_pos_ij = sdl_geometry
            .ij_to_sdl
            .inverse_transform_point(&mouse_pos);
        let key = [mouse_pos_ij.x.floor() as i16,
                   mouse_pos_ij.y.floor() as i16];
        match self.points.get(&key) {
            None => { self.points.insert(key, 1); },
            _ => { self.points.remove(&key); },
        }
    }

    pub fn draw(
        &self,
        view: &mut View,
        mouse_state: &MouseState,
    ) {
        let sdl_geometry = self.sdl_geometry(&view);
        let width = sdl_geometry.window_size.x;
        let height = sdl_geometry.window_size.y;

        let max_dimension: f32 = width.hypot(height);

        let mouse_pos = Point2::new(mouse_state.x() as f32,
                                    mouse_state.y() as f32);
        let mouse_pos_ij = sdl_geometry
            .ij_to_sdl
            .inverse_transform_point(&mouse_pos);


        // first we draw the grid. this also involves drawing the cursor
        // highlight, but we want to layer it later, so we do it in this
        // `Texture` instead.
        let textures = view.textures();
        let mut highlight = textures.create_texture_target(
            LAYER_PIXEL_FORMAT,
            width as u32,
            height as u32,
        ).unwrap();
        highlight.set_blend_mode(BlendMode::Blend);
        clear_texture(&mut highlight, &mut view.canvas);

        let grid_nw = sdl_geometry.origin
            - Vector2::new(max_dimension, max_dimension);
        let grid_se = sdl_geometry.origin
            + Vector2::new(max_dimension, max_dimension);
        let n_lines: i32 = (max_dimension/sdl_geometry.total_scale)
            .round() as i32;
        // the above is a little misleading; it's half the amount of gridlines
        // that get drawn on each axis, and a quarter of the amount of lines
        // that get drawn overall.
        for this_i in -n_lines .. n_lines {
            // the i that this is looping over is in fact the i coordinate in
            // our ij coordinate system. each set of diagonals is placed one i
            // over from the previous one. this part of the algorithm is
            // translationally symmetric with a period of one i, so it doesn't
            // matter where the origin is. or at least, that _would_ be the
            // case if it weren't for the fact that we have the mouse to worry
            // about. so we're forced to have an origin after all. `ij_to_xy()`
            // puts it at the top left corner, which means that all these lines
            // have fractional ij coordinates, which is a little upsetting. but
            // if you subtract (origin_i, origin_j), then they should come out
            // to integers. if you do that, then at every step, we are drawing:
            //   the line at i = this_i
            //   the line at j = -this_i
            let offset: f32 = SQRT_2
                * (this_i as f32)
                * sdl_geometry.total_scale;
            let this_x_nw = grid_nw.x + offset;
            let this_x_se = grid_se.x + offset;
            // this is the positive slope line at i = this_i
            if (mouse_pos_ij.x - this_i as f32).abs() < 1. {
                view.canvas.with_texture_canvas(
                    &mut highlight,
                    |c| {
                        c.thick_line::<Color>(
                            this_x_nw as i16,
                            grid_nw.y as i16,
                            this_x_se as i16,
                            grid_se.y as i16,
                            3, color::PALETTE[4],
                        )
                            .expect("Cannot draw highlight.");
                    }
                )
                    .expect("Cannot draw highlight.");
            } else {
                view.canvas.line::<Color>(
                    this_x_nw as i16,
                    grid_nw.y as i16,
                    this_x_se as i16,
                    grid_se.y as i16,
                    color::PALETTE[1]
                )
                    .expect("Cannot draw line.");
            }
            // this is the negative slope line at j = -this_i
            if (mouse_pos_ij.y + this_i as f32).abs() < 1. {
                view.canvas.with_texture_canvas(
                    &mut highlight,
                    |c| {
                        c.thick_line::<Color>(
                            this_x_se as i16,
                            grid_nw.y as i16,
                            this_x_nw as i16,
                            grid_se.y as i16,
                            3, color::PALETTE[4]
                        )
                            .expect("Cannot draw highlight.");
                    }
                )
                    .expect("Cannot draw highlight.");
            } else {
                view.canvas.line::<Color>(
                    this_x_se as i16,
                    grid_nw.y as i16,
                    this_x_nw as i16,
                    grid_se.y as i16,
                    color::PALETTE[1]
                )
                    .expect("Cannot draw line.");
            }
        }

        // now we draw all the `Patch`es.
        for (ij, c) in self.points.iter() {
            let this_point = sdl_geometry
                .ij_to_sdl
                .transform_point(
                    &Point2::new(ij[0] as f32, ij[1] as f32)
                );
            if grid_nw.x <= this_point.x
                && this_point.x <= grid_se.x
                && grid_nw.y <= this_point.y
                && this_point.y <= grid_se.y {
                let (x_array, y_array) = xy_to_diamond(
                    this_point,
                    sdl_geometry.total_scale,
                );
                view.canvas.filled_polygon(&x_array, &y_array, color::PALETTE[*c])
                    .expect("Cannot draw box.");
            }
        }

        // drawing all the deferred layers (so far just the highlight).
        view.canvas.set_blend_mode(BlendMode::Blend);
        view.canvas.copy(&highlight, None, None)
            .expect("Cannot copy highlight to canvas.");
    }
}
