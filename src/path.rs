use crate::na;
use na::{
    Point2,
};

use crate::sdl2;
use sdl2::gfx::primitives::DrawRenderer;

use crate::color;
use crate::View;

pub enum PathType {
    PiecewiseLinear,
}

pub struct Path {
    path_type: PathType,
    points: Vec<Point2<f32>>,
    color: usize,
}

impl Path {
    fn x_array(&self) -> &[i16] {
        &self.points.iter().map(|p| p.x as i16).collect::<[i16]>()
    }
    fn y_array(&self) -> &[i16] {
    }
    pub fn draw(&self, view: &mut View) {
        match self.path_type {
            PathType::PiecewiseLinear => {
                view.canvas.filled_polygon(
                    self.x_array(),
                    self.y_array(),
                    color::PALETTE[self.color],
                )
                    .expect("Could not draw path.");
            }
            _ => {}
        }
    }
}
