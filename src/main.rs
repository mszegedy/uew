extern crate nalgebra as na;
extern crate tiny_skia as ts;
extern crate sdl2;

use na::{
    Point2,
    Vector2,
};

use tiny_skia::{
    Pixmap,
    PixmapRef,
};

use sdl2::surface::Surface;
use sdl2::pixels::{
    Color,
    PixelFormatEnum,
};
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use std::time::Duration;

mod color;
mod path;
mod layer;

pub struct View {
    pixmap: Pixmap,
    origin: Vector2<f32>,
    scale: f32,
}

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let window = video_subsystem
        .window("Uralic Embroidery Workshop", 800, 600)
        .position_centered()
        .resizable()
        .borderless()
        .build()
        .unwrap();
    let mut view = View {
        pixmap: Pixmap::new(),
        origin: Vector2::new(0f32, 0f32),
        scale: 1f32,
    };
    let mut view_surface = Surface::from_data(
        &mut view.pixmap.as_mut().data_mut(),
        pixmap.width(),
        pixmap.height(),
        pixmap.width() * 4, // meant to be the length of a scanline in bytes
        PixelFormat::ARGB8888, // small caveat: skia's bytes are premultiplied
    );
    let mut layer = layer::Layer::new(Point2::new(0f32, 0f32), 64f32);
    layer.add_point(0, 0, 1);

    view.canvas.set_blend_mode(BlendMode::Blend);
    view.canvas.set_draw_color(Color::RGB(0, 0, 0));
    view.canvas.clear();
    view.canvas.present();

    let mut event_pump = sdl_context.event_pump().unwrap();
    'running: loop {
        view.canvas.set_draw_color(Color::RGB(0, 0, 0));
        view.canvas.clear();
        let mouse_state = event_pump.mouse_state();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..}
                | Event::KeyDown { keycode: Some(Keycode::Q), .. } => {
                    break 'running
                },
                Event::KeyDown { keycode: Some(keycode), .. } => {
                    match keycode {
                        Keycode::Period => {
                            if (view.scale + 0.15) < 3f32 {
                                view.scale += 0.15;
                            }
                        },
                        Keycode::Comma => {
                            if (view.scale - 0.15) > 0f32 {
                                view.scale -= 0.15;
                            }
                        },
                        Keycode::Up => {
                            view.origin.y += 16f32;
                        },
                        Keycode::Left => {
                            view.origin.x += 16f32;
                        },
                        Keycode::Down => {
                            view.origin.y -= 16f32;
                        },
                        Keycode::Right => {
                            view.origin.x -= 16f32;
                        },
                        // the keycodes behave weirdly with dbus and stuff so sometimes
                        // we have to inspect them (e.g. this is how we found out that
                        // the programmer's dvorak plus sign comes up as the "9"
                        // keycode)
                        _ => { println!("{}", keycode.name()); },
                    }
                },
                Event::MouseButtonUp { .. } => {
                    layer.click(
                        &mut view,
                        &mouse_state,
                    );
                }
                _ => {}
            }
        }
        layer.draw(
            &mut view,
            &mouse_state,
        );
        view.canvas.present();
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
}
